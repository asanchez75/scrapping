/*
 * @author:         Carlo Fontanos
 * @author_url:     carlofontanos.com
 * 
 * A simple code snippet for parsing JSON data from a URL
 */

package scrapping;                
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonParser {
    
    public static void main(String[] args) {
        JSONParser parser = new JSONParser();

        try {
            String content = "Berlin";
            URL dbpedia = new URL("http://spotlight.sztaki.hu:2222/rest/annotate?text=" + content + "&confidence=0.5&support=0&spotter=Default&disambiguator=Default&policy=whitelist&types=&sparql="); // URL to Parse
            URLConnection yc = dbpedia.openConnection();
            

            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
            
            String inputLine;
            while ((inputLine = in.readLine()) != null) {               
              System.out.println(inputLine);
            }
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }   
    

}