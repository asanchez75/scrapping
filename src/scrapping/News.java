package scrapping;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;

import com.jaunt.Element;
import com.jaunt.Elements;
import com.jaunt.NotFound;
import com.jaunt.ResponseException;
import com.jaunt.UserAgent;

public class News {
 
  public static String directory = "/Users/asanchez75/Documents/workspace/News/http:/www.bbc.co.uk/news/";
  
  public static void main(String[] args) throws Exception {
    
    frequencies();
  }

  /**
   * It computes frequency of words by file
   * @throws Exception 
   */
  public static void frequencies() throws Exception {
    
    ArrayList<String> files = listOfFiles();


    for (String filename : files) {
      TreeMap<String, Integer> occurrences = new TreeMap<String, Integer>();

      String text = FileUtils.readFileToString(new File(directory + filename), "UTF-8");
      // removing all unnecessary characters
      String cleaned_text = text.replaceAll("\\P{L}+", " ");
      // removing stop words
      StringBuilder stopwords_text = removeStopWords(cleaned_text.toLowerCase());
      StringTokenizer strings = new StringTokenizer(stopwords_text.toString(), " ");
      while (strings.hasMoreElements()) {
        String word = strings.nextToken();
        Integer oldCount = occurrences.get(word);
        if ( oldCount == null ) {
           oldCount = 0;
        }
        occurrences.put(word, oldCount + 1);
      
      }
      
      SortedSet<Entry<String, Integer>> frequencies = entriesSortedByValues(occurrences);
      Iterator<Entry<String, Integer>> it = frequencies.iterator();
      while (it.hasNext()) {
        Entry<String, Integer> frequency = it.next();
        if (!frequency.getKey().contains(" ") && !frequency.getKey().isEmpty()) {
          String line = frequency.getKey() +  "|" + frequency.getValue();
          FileUtils.writeStringToFile(new File("tokenization/" + filename), line + "\n", true);
        }
      }
      
    }
  }
  
  /**
   * Remove stopwords given a text
   * @param textFile
   * @return
   * @throws Exception
   */
  public static StringBuilder removeStopWords(String textFile) throws Exception {
    
    CharArraySet stopWords = EnglishAnalyzer.getDefaultStopSet();
    CharArraySet stopSet = CharArraySet.copy(Version.LUCENE_44, stopWords);
    stopSet.addAll(stopWords());
    TokenStream tokenStream = new StandardTokenizer(Version.LUCENE_44, new StringReader(textFile));

    tokenStream = new StopFilter(Version.LUCENE_44, tokenStream, stopSet);
    StringBuilder sb = new StringBuilder();
    CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
    
    tokenStream.reset();
    
    while (tokenStream.incrementToken()) {
        String term = charTermAttribute.toString();
        sb.append(term + " ");
    }
    
    return sb;
}
  
  /**
   * Generates a set of files. Each file contains dbpedia uris 
   * @throws IOException
   * @throws NotFound
   * @throws ResponseException
   */
  public static void annotate() throws IOException, NotFound, ResponseException {
    
    ArrayList<String> files = listOfFiles();
    
    for (String filename : files) {
      HashSet<String> uris = new HashSet<String>();
      List<String> lines = FileUtils.readLines(new File(directory + filename), "UTF-8");
      for (String line : lines) {
        if (!line.isEmpty()) {
          generateAnnotations(line, filename, uris);
          System.out.println(uris.toString());
        }
      }
      FileUtils.writeLines(new File("annotations/" + filename), "UTF-8", uris, true);
    }
    
  }
  

  
  /**
   * Extract set of uris given a text from dbpedia spotlight
   * @param text
   * @param filename
   * @param uris
   * @throws ResponseException
   * @throws NotFound
   * @throws IOException
   */
  public static void generateAnnotations(String text, String filename, HashSet<String> uris) throws ResponseException, NotFound, IOException {
    String Url = "http://spotlight.sztaki.hu:2222/rest/annotate?text=" + URLEncoder.encode(text) + "&confidence=0.5&support=0&spotter=Default&disambiguator=Default&policy=whitelist&types=&sparql="; 
    UserAgent userAgent = new UserAgent(); 
    userAgent.visit(Url);
    //userAgent.doc.findEvery("<textarea>");
    Elements elements = userAgent.doc.findEvery("<body>").findEvery("<a>"); 
    for (Element element : elements) {
      uris.add(element.getAt("href"));
    }
  }
  
  /**
   * Get list of filenames from folder containing scrapped text
   * @return
   */
  public static ArrayList<String> listOfFiles() {
    ArrayList<String> list = new ArrayList<String>();
    File folder = new File(directory);
    File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
          if (listOfFiles[i].isFile()) {
            list.add(listOfFiles[i].getName());
          }
        }
    return list;
  }
  
  
  /**
   * Step 1 - Scrapping news from BBC 
   * @throws ResponseException
   * @throws NotFound
   * @throws IOException
   */
  public static void getNews() throws ResponseException, NotFound, IOException {
    
    UserAgent userAgent = new UserAgent(); 
    
   for (int i = 1; i < 51; i++) {
     System.out.println("page " + i);
     userAgent.visit(link(i));
     Elements links = userAgent.doc.findEvery("<h1 itemprop=\"headline\">").findEvery("<a>");  //find search result links
     for(Element link : links) {
       
       UserAgent userAgent2 = new UserAgent();
       
       userAgent2.visit(link.getAt("href"));
       
       String title = userAgent2.doc.findEvery("<span class=\"cta\">").innerText();
       String body = userAgent2.doc.findEvery("<div class=\"story-body__inner\">").innerText();

       FileUtils.writeStringToFile(new File(link.getAt("href")), title +  "\n" + body, true);
       }
    
   }  
  }
  
  public static String link(int pagenumber) {
    return "http://www.bbc.co.uk/search/more?page=" + pagenumber + "&q=olympics+games+Rio+2016&filter=news";
  }
  
  
  /**
   * To sort a treemap by values
   * @param map
   * @return
   */
  static <K,V extends Comparable<? super V>>
  SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
      SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
          new Comparator<Map.Entry<K,V>>() {
              @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
                  int res = e2.getValue().compareTo(e1.getValue());
                  return res != 0 ? res : 1;
              }
          }
      );
      sortedEntries.addAll(map.entrySet());
      return sortedEntries;
  }
  
  /**
   * Add additional stop words
   * @return
   * @throws IOException
   */
  public static List<String> stopWords() throws IOException {
    List<String> lines = FileUtils.readLines(new File("stopwords/stopwordlist.txt"), "UTF-8");
    return lines;
  }
  
}
