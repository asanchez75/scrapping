package scrapping;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.ar.ArabicAnalyzer;
import org.apache.lucene.analysis.cjk.CJKAnalyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.TokenizerFactory;

public class TokenizationByLanguage {
  public static String directory = "/Users/asanchez75/Documents/workspace/News/recodefortokenization/";
  public static void main(String[] args) throws IOException {

    ArrayList<String> files = listOfFiles();

    for (String filename : files) {
      String text = FileUtils.readFileToString(new File(directory + filename), "UTF-8");
      
      Analyzer analyzer = new EnglishAnalyzer(); // or any other analyzer
      //Analyzer analyzer = new RussianAnalyzer(); // or any other analyzer
     //Analyzer analyzer = new CJKAnalyzer(); // or any other analyzer
     //Analyzer analyzer = new ArabicAnalyzer(); // or any other analyzer
     
      TokenStream ts = analyzer.tokenStream("", new StringReader(text));
      // The Analyzer class will construct the Tokenizer, TokenFilter(s), and CharFilter(s),
      //   and pass the resulting Reader to the Tokenizer.
      CharTermAttribute termAttrib = (CharTermAttribute) ts.getAttribute(CharTermAttribute.class);
      try {
        ts.reset(); // Resets this stream to the beginning. (Required)
        while (ts.incrementToken()) {
          // Use AttributeSource.reflectAsString(boolean)
          // for token stream debugging.
          FileUtils.writeStringToFile(new File("recodefortokenization/tokenization/" + filename), termAttrib.toString() + "\n", true);
        }
        ts.end();   // Perform end-of-stream operations, e.g. set the final offset.
      } finally {
        ts.close(); // Release resources associated with this stream.
      }
      
    }
    
  }
  
 
   /**
    * Get list of filenames from folder containing scrapped text
    * @return
    */
   public static ArrayList<String> listOfFiles() {
     ArrayList<String> list = new ArrayList<String>();
     File folder = new File(directory);
     File[] listOfFiles = folder.listFiles();
  
         for (int i = 0; i < listOfFiles.length; i++) {
           if (listOfFiles[i].isFile()) {
             list.add(listOfFiles[i].getName());
           }
         }
     return list;
   }
}
