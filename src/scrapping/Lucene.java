package scrapping;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;




public class Lucene {

  public static void main(String[] args) throws Exception {
    
    String textFile = "The big fat Cat, said 'your funniest guy i know' to the kangaroo...";
    StringBuilder output = removeStopWords(textFile.toLowerCase());
    System.out.println(output.toString());
  }
  
  public static StringBuilder removeStopWords(String textFile) throws Exception {
    
    CharArraySet stopWords = EnglishAnalyzer.getDefaultStopSet();
    CharArraySet stopSet = CharArraySet.copy(Version.LUCENE_44, stopWords);
    stopSet.addAll(stopWords());
    TokenStream tokenStream = new StandardTokenizer(Version.LUCENE_44, new StringReader(textFile));

    tokenStream = new StopFilter(Version.LUCENE_44, tokenStream, stopSet);
    StringBuilder sb = new StringBuilder();
    CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
    
    tokenStream.reset();
    
    while (tokenStream.incrementToken()) {
        String term = charTermAttribute.toString();
        sb.append(term + " ");
    }
    
    return sb;
}

  public static List<String> stopWords() throws IOException {
    List<String> lines = FileUtils.readLines(new File("stopwords/stopwordlist.txt"), "UTF-8");
    return lines;
  }
  
}
